from Common.Algorithm import Algorithm
from Common.MySystem import MySystem
from Common.Core import genEvent
from Common.Statistics import Execution
import random
import copy


class MyGA(Algorithm):
    def __init__(self):
        Algorithm.__init__(self)
        self.population = []
        self.currentIter = 0

    def Step(self):
        self._select()
        self._recombine()
        self._mutate()
        self._evalPopulation()

    def Run(self):
        self.Clear()
        Algorithm.timecounts = 0
        Algorithm.simcounts = 0
        for i in range(self.algconf.popNum):
            s = MySystem()
            s.GenerateRandom(20)
            self.population.append(s)
        if Algorithm.algconf.metamodel:
            raise Exception("Metamodels are not implemented")
        self.population.sort(key=lambda x: x.rel * x.penalty, reverse=True)
        for self.currentIter in range(self.algconf.maxIter):
            self.Step()
            if self.population[0].CheckConstraints() and (
                            self.currentSolution is None or self.population[0].rel > self.currentSolution.rel):
                self.currentSolution = copy.deepcopy(self.population[0])
                # self.currentSolution.Update(use_metamodel=False)
            print(self.currentIter, self.currentSolution)
        print("--------------------------------------\n")
        self.heuristic()
        print("After heuristic", self.currentSolution)
        self.stat.AddExecution(Execution(self.currentSolution, self.currentIter, float("NaN"), Algorithm.timecounts,
                                         Algorithm.simcounts))

    def Clear(self):
        Algorithm.Clear(self)
        self.population = []
        self.currentIter = 0

    def _select(self):
        self.firstParent = []
        self.secondParent = []
        parents_num = int(self.algconf.popNum * self.algconf.crossPercent / 2)
        for i in range(parents_num):
            tournament = []
            for j in range(self.algconf.tournamentSize):
                tournament.append(self.population[random.randint(0, self.algconf.popNum - 1)])
            tournament.sort(key=lambda x: x.rel * x.penalty, reverse=True)
            self.firstParent.append(tournament[0])
        if self.algconf.roulette:
            probabilities = []
            sum_val = 0.0
            for s in self.population:
                val = s.rel * s.penalty
                sum_val += val
                probabilities.append(val)
            for p in range(self.algconf.popNum):
                probabilities[p] = probabilities[p] / sum_val
            nums = range(self.algconf.popNum)
            events = dict(zip(nums, probabilities))
            for i in range(parents_num):
                self.secondParent.append(self.population[genEvent(events)])
        else:
            self.secondParent = copy.deepcopy(self.population[:parents_num])

    def _recombine(self):
        self.population = []
        parent_size = len(self.firstParent)
        for i in range(parent_size):
            self.population.append(self.firstParent[i])
            self.population.append(self.secondParent[i])
            child1 = copy.deepcopy(self.firstParent[i])
            child2 = copy.deepcopy(self.secondParent[i])
            cross_points = random.sample(range(parent_size), 2)
            cross_points.sort()
            child1.modules = self.firstParent[i].modules[0:cross_points[0]] + \
                             self.secondParent[i].modules[cross_points[0]:cross_points[1]] + \
                             self.firstParent[i].modules[cross_points[1]:]
            child2.modules = self.secondParent[i].modules[0:cross_points[0]] + \
                             self.firstParent[i].modules[cross_points[0]:cross_points[1]] + \
                             self.secondParent[i].modules[cross_points[1]:]
            child1.Update()
            child2.Update()
            self.population.append(child1)
            self.population.append(child2)

    def _mutate(self):
        for s in self.population:
            for i in range(len(s.modules)):
                if random.random() < self.algconf.mutPercent:
                    prev_val = s.modules[i].num
                    new_val = random.randint(s.modules[i].min_num, s.modules[i].max_num)
                    s.modules[i].num = int(prev_val + (new_val - prev_val)*(1 - self.currentIter / self.algconf.maxIter))

    def _evalPopulation(self):
        self.population.sort(key=lambda x: x.rel * x.penalty, reverse=True)
        self.population = self.population[:self.algconf.popNum]

    def heuristic(self):
        solution = self.currentSolution
        while solution.CheckConstraints:
            self.currentSolution = solution
            temp = 0.0
            index = -1
            for i in range(len(solution.modules)):
                if solution.modules[i].num == solution.modules[i].max_num:
                    continue
                tmp_sol = copy.deepcopy(solution)
                tmp_sol.modules[i].num += 1
                tmp_sol.Update()
                for j in range(solution.constraints_num):
                    delta = (tmp_sol.rel - solution.rel)/solution.coeff[j][i]
                    if delta > temp:
                        temp = delta
                        index = i
            if index == -1:
                break
            solution.modules[index].num += 1
